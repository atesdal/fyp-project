# FYP Games Programming at Teesside University
## Adrian Tesdal <actesdal@gmail.com>
## Student 2016-2019

Final year project for BSc Games Programming, goal is eventually to support
Linux and Windows, but for setup purposes and not wanting to take on more than I
can handle it will only be supported on Windows to start with.

Requires 32bit SFML-2.5.1 binaries, that can be downloaded [here](https://www.sfml-dev.org/download/sfml/2.5.1/).