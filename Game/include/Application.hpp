#include "../../Engine/include/MessengerModule.hpp"

#include <thread>
#include <vector>

class MessengerEngine;
class GraphicsObject;

class Application : public MessengerModule
{
public:
	Application();
	~Application();

	void Run();

private:
	void Update();
	void Interpret(Message &m) override;
	void HandleKeyInput(Message &m);
	void HandleWindowEvent(Message &m);
	void HandleGraphicInit(Message &m);

private:
	std::thread engine_thread_;
	bool game_running_{ true };
	std::vector<GraphicsObject*> objects_;
};