#ifndef GRAPHICS_OBJECT_HPP
#define GRAPHICS_OBJECT_HPP

class Messenger;

class GraphicsObject
{
public:
	enum class Shape {
		Circle,
		Square
	};

	enum class Colour {
		Red,
		Green,
		Blue,
		White
	};

	GraphicsObject(
		Messenger *messenger,
		Shape shape,
		float pos_x,
		float pos_y,
		float size,
		Colour colour,
		int index);
	~GraphicsObject();


	void SetHandle(unsigned int handle) { handle_ = handle; }
	void Initialised() { initialised_ = true; }
	bool IsInitialised() const { return initialised_; }
	void SetActive(bool status);
	bool IsActive() const { return is_active_; }
	void Move(float dir_x, float dir_y);

private:
	Messenger *messenger_;
	unsigned int handle_{ 0 };
	bool initialised_, is_active_;
};

#endif // !GRAPHICS_OBJECT_HPP