#include "GraphicsObject.hpp"
#include "Messenger.hpp"
#include "Message.hpp"
#include <string>

GraphicsObject::GraphicsObject(
		Messenger *messenger,
		Shape shape,
		float pos_x,
		float pos_y,
		float size,
		Colour colour,
		int index) :
	messenger_(messenger), initialised_(false), is_active_(true)
{
	Message m(Message::Type::GraphicInit);
	if (shape == Shape::Circle) {
		m.AddArg("circle");
	}
	else {
		m.AddArg("square");
	}
	m.AddArg(std::to_string(pos_x));
	m.AddArg(std::to_string(pos_y));
	m.AddArg(std::to_string(size));
	switch (colour) {
		case Colour::Red:
			m.AddArg("255");
			m.AddArg("0");
			m.AddArg("0");
			break;
		case Colour::Green:
			m.AddArg("0");
			m.AddArg("255");
			m.AddArg("0");
			break;
		case Colour::Blue:
			m.AddArg("0");
			m.AddArg("0");
			m.AddArg("255");
			break;
		case Colour::White:
			m.AddArg("255");
			m.AddArg("255");
			m.AddArg("255");
			break;
	}
	m.AddArg(std::to_string(index));
	messenger_->Send(ModuleName::Engine, m);
}

GraphicsObject::~GraphicsObject()
{
}

void
GraphicsObject::SetActive(bool status)
{
	Message m(Message::Type::GraphicFeedback);
	if (status) { m.AddArg("activate"); }
	else { m.AddArg("deactivate"); }
	m.AddArg(std::to_string(handle_));
	messenger_->Send(ModuleName::Engine, m);
	is_active_ = status;
}

void
GraphicsObject::Move(float dir_x, float dir_y)
{
	Message m(Message::Type::GraphicFeedback);
	m.AddArg("move");
	m.AddArg(std::to_string(handle_));
	m.AddArg(std::to_string(dir_x));
	m.AddArg(std::to_string(dir_y));
	messenger_->Send(ModuleName::Engine, m);
}
