#include "Application.hpp"

#include "MessageEngine_fwd.hpp"
#include "GraphicsObject.hpp"
#include <thread>

Application::Application() :
	MessengerModule(ModuleName::Application)
{
}

Application::~Application()
{
	for (auto &object : objects_) {
		delete object;
	}
}

void
Application::Run()
{
	MessageEngine *e = new MessageEngine(messenger_);

	engine_thread_ = std::thread(&Engine::Init, e, 800, 600, "SFML");

	GraphicsObject *square = new GraphicsObject(
		messenger_,
		GraphicsObject::Shape::Square,
		20.0f, 20.0f,
		10.0f,
		GraphicsObject::Colour::Green,
		objects_.size());
	objects_.push_back(square);

	GraphicsObject *circle = new GraphicsObject(
		messenger_,
		GraphicsObject::Shape::Circle,
		40.0f, 40.0f,
		10.0f,
		GraphicsObject::Colour::Red,
		objects_.size());
	objects_.push_back(circle);

	Update();
}

void
Application::Update()
{
	while (game_running_) {
		ReceiveAll();
	}
}

void
Application::Interpret(Message &m)
{
	switch (m.GetType()) {
		case Message::Type::KeyInput:
			HandleKeyInput(m);
			break;
		case Message::Type::WindowEvent:
			HandleWindowEvent(m);
			break;
		case Message::Type::GraphicInit:
			HandleGraphicInit(m);
			break;
	}
}

void
Application::HandleKeyInput(Message &m)
{
	auto &args = m.GetArgs();
	Message message(Message::Type::GraphicFeedback);
	if (args.front() == "keyw") {
		objects_[0]->Move(0.0f, -10.0f);
	}
	else if (args.front() == "keys") {
		objects_[0]->Move(0.0f, 10.0f);
	}
	else if (args.front() == "keya") {
		objects_[1]->Move(-10.0f, 0.0f);
	}
	else if (args.front() == "keyd") {
		objects_[1]->Move(10.0f, 0.0f);
	}
	else if (args.front() == "keyd") {
		objects_[1]->Move(10.0f, 0.0f);
	}
	else if (args.front() == "key1") {
		objects_[1]->SetActive(false);
	}
	else if (args.front() == "key2") {
		objects_[1]->SetActive(true);
	}
}

void
Application::HandleWindowEvent(Message &m)
{
	//expected format: command
	auto &args = m.GetArgs();
	Message msg(Message::Type::WindowFeedback);
	if (args.front() == "windowclose") {
		msg.AddArg("quit");
	}
	else if (args.front() == "closed") {
		game_running_ = false;
		engine_thread_.join();
		return;
	}
	messenger_->Send(ModuleName::Engine, msg);
}

void
Application::HandleGraphicInit(Message &m)
{
	//expected format: index, handle
	auto &args = m.GetArgs();
	objects_[std::stoi(args[0])]->SetHandle(std::stoi(args[1]));
	objects_[std::stoi(args[0])]->Initialised();
}

