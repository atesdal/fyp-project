## 22nd of January
Managed to set up the VS solution to have 3 different projects using static
linking and have it seemingly work fine. Will almost definitely have to add more
to take the message-based structure into account, but as a start being able to
build the introductionary SFML program using a structure of:
Engine->Game->fyp-project
Where SFML is contained within the Engine project seems like a decent start to
me. Especially considering I will need to consult with several lecturers and do
a lot of research on how this project should be set up, I do not want to start
doing massive amounts of work on the code itself since I have no structure
planned out.
