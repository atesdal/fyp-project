#include "Message.hpp"

Message::Message()
{
}

Message::Message(Type t) :
	t_(t)
{
}

Message::Message(Type t, const std::vector<std::string> &args) :
	t_(t)
{
	args_ = args;
	argc_ = args.size();
}

Message::~Message()
{
}

void
Message::AddArg(std::string arg)
{
	args_.push_back(arg);
	argc_++;
}
