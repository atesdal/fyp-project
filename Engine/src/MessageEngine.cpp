#include "MessageEngine.hpp"
#include "RenderObject.hpp"
#include <SFML/Graphics.hpp>

#include <iostream>
#include <cstdlib>

MessageEngine::MessageEngine(Messenger *parent) :
	MessengerModule(ModuleName::Engine)
{
	messenger_->AddRecipient(ModuleName::Application, parent);
	parent->AddRecipient(ModuleName::Engine, messenger_);
}

MessageEngine::~MessageEngine()
{
}

void
MessageEngine::Update()
{
	while (window_->isOpen()) {
		sf::Event e;
		while (window_->pollEvent(e)) {
			PackEvent(e);
		}
		window_->clear();
		Draw();
		window_->display();
		ReceiveAll();
	}
	//if window is closed
	delete this;
}

void
MessageEngine::Interpret(Message &m)
{
	switch (m.GetType()) {
		case Message::Type::GraphicFeedback:
			HandleGraphicFeedback(m);
			break;
		case Message::Type::WindowFeedback:
			HandleWindowFeedback(m);
			break;
		case Message::Type::GraphicInit:
			HandleGraphicInit(m);
			break;
		default:
			std::cerr << "Engine received incompatible message type\n";
			break;
	}
}

void
MessageEngine::HandleGraphicFeedback(Message &m)
{
	//expected message format: command,renderobject handle, *args
	auto &args = m.GetArgs();
	if (args.front() == "move") {
		renderobjects_[std::stoi(args[1]) - 1]->Translate(std::stof(args[2]), std::stof(args[3]));
	}
	else if (args.front() == "deactivate") {
		renderobjects_[std::stoi(args[1]) - 1]->SetActive(false);
	}
	else if (args.front() == "activate") {
		renderobjects_[std::stoi(args[1]) - 1]->SetActive(true);
	}
}

void
MessageEngine::HandleWindowFeedback(Message &m)
{
	//expected message format: command
	auto &args = m.GetArgs();
	if (args.front() == "quit") {
		Message m(Message::Type::WindowEvent);
		m.AddArg("closed");
		messenger_->Send(ModuleName::Application, m);
		Close();
	}
}

void
MessageEngine::HandleGraphicInit(Message &m)
{
	//expected format: shape, posx, posy, size, rgb, index
	auto &args = m.GetArgs();
	unsigned int handle{ 0 };
	if (args[0] == "circle") {
		AddCircleShape(
			handle,
			std::stof(args[1]),
			std::stof(args[2]),
			std::stof(args[3]),
			std::stoi(args[4]),
			std::stoi(args[5]),
			std::stoi(args[6]));
	}
	else {
		AddRectangleShape(
			handle,
			std::stof(args[1]),
			std::stof(args[2]),
			std::stof(args[3]),
			std::stoi(args[4]),
			std::stoi(args[5]),
			std::stoi(args[6]));
	}
	Message message(Message::Type::GraphicInit);
	message.AddArg(args[7]);
	message.AddArg(std::to_string(handle));
	messenger_->Send(ModuleName::Application, message);
}

void
MessageEngine::PackEvent(sf::Event &e)
{
	switch (e.type) {
		case sf::Event::Closed:
		{
			Message m(Message::Type::WindowEvent);
			m.AddArg("windowclose");
			messenger_->Send(ModuleName::Application, m);
			break;
		}
		case sf::Event::KeyPressed:
		{
			Message m(Message::Type::KeyInput);
			switch (e.key.code) {
				case sf::Keyboard::W:
					m.AddArg("keyw");
					break;
				case sf::Keyboard::A:
					m.AddArg("keya");
					break;
				case sf::Keyboard::S:
					m.AddArg("keys");
					break;
				case sf::Keyboard::D:
					m.AddArg("keyd");
					break;
				case sf::Keyboard::Num1:
					m.AddArg("key1");
					break;
				case sf::Keyboard::Num2:
					m.AddArg("key2");
					break;
				default:
					return;
			}
			messenger_->Send(ModuleName::Application, m);
		}
	}
}
