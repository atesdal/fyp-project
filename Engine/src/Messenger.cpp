#include "Messenger.hpp"

Messenger::Messenger()
{
}

Messenger::~Messenger()
{
}

void
Messenger::Send(ModuleName recipient, Message &message)
{
	recipients_.at(recipient)->Receive(message);
}

void
Messenger::Receive(Message &message)
{
	std::lock_guard<std::mutex> lock(m_);
	inbox_.push(message);
	c_.notify_one();
}

void
Messenger::Read(Message &message)
{
	std::unique_lock<std::mutex> lock(m_);
	while (inbox_.empty()) { c_.wait(lock); }
	message = inbox_.front();
	inbox_.pop();
}

bool
Messenger::TryRead(Message &message)
{
	std::unique_lock<std::mutex> lock(m_);
	if (inbox_.empty()) { return false; }
	message = inbox_.front();
	inbox_.pop();
	return true;
}

void
Messenger::AddRecipient(ModuleName name, Messenger *recipient)
{
	recipients_[name] = recipient;
}
