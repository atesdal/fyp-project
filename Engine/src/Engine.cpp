#include "Engine.hpp"

#include "RenderObject.hpp"

#include <SFML/Graphics.hpp>

#include <iostream>
#include <thread>

Engine::Engine()
{
}

Engine::~Engine()
{
	delete window_;
	for (const auto &object : renderobjects_) {
		delete object;
	}
}

void
Engine::Init(
	unsigned short int w_width,
	unsigned short int w_height,
	const char *w_name)
{
	window_ = 
		new sf::RenderWindow(
			sf::VideoMode(w_width, w_height),
			w_name);
	Update();
}

void
Engine::Close()
{
	window_->close();
}

void
Engine::AddRectangleShape(
	unsigned int &handle,
	float pos_x,
	float pos_y,
	float size,
	int r,
	int g,
	int b)
{
	RenderObject *rect = new RenderObject();
	rect->InitSquareShape(pos_x, pos_y, size, sf::Color(r, g, b, 255));
	renderobjects_.push_back(rect);
	handle = renderobjects_.size();
}

void
Engine::AddCircleShape(
	unsigned int &handle,
	float pos_x,
	float pos_y,
	float circle_radius,
	int r,
	int g,
	int b)
{
	RenderObject *rect = new RenderObject();
	rect->InitCircleShape(pos_x, pos_y, circle_radius, sf::Color(r, g, b, 255));
	renderobjects_.push_back(rect);
	handle = renderobjects_.size();
}

void
Engine::Draw()
{
	for (const auto &object : renderobjects_) {
		if (object->IsActive()) {
			object->Draw(window_);
		}
	}
}
