#include "MessengerModule.hpp"

MessengerModule::MessengerModule(ModuleName name) :
	name_(name)
{
	messenger_ = new Messenger();
}

MessengerModule::~MessengerModule()
{
	delete messenger_;
}

void
MessengerModule::Receive()
{
	Message m;
	if (messenger_->TryRead(m)) {
		Interpret(m);
	}
}

void
MessengerModule::ReceiveAll()
{
	Message m;
	while (messenger_->TryRead(m)) {
		Interpret(m);
	}
}
