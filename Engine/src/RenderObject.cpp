#include "RenderObject.hpp"

#include <SFML/Graphics.hpp>
#include <iostream>

RenderObject::RenderObject() :
	is_active_(true)
{
}


RenderObject::~RenderObject()
{
	delete shape_;
}

void
RenderObject::InitCircleShape(float pos_x, float pos_y, float circle_radius, sf::Color colour)
{
	shape_ = new sf::CircleShape(circle_radius);
	shape_->setPosition(sf::Vector2f(pos_x, pos_y));
	shape_->setFillColor(colour);
}

void
RenderObject::InitSquareShape(float pos_x, float pos_y, float size, sf::Color colour)
{
	shape_ = new sf::RectangleShape(sf::Vector2f(size, size));
	shape_->setPosition(sf::Vector2f(pos_x, pos_y));
	shape_->setFillColor(colour);
}

void
RenderObject::Draw(sf::RenderWindow *window) const
{
	if (is_active_) { window->draw((*shape_)); }
}

bool
RenderObject::Translate(float dir_x, float dir_y) const
{
	shape_->move(sf::Vector2f(dir_x, dir_y));
	return true;
}

void
RenderObject::SetActive(bool status)
{
	is_active_ = status;
}

sf::Vector2f RenderObject::GetPos() const
{
	return shape_->getPosition();
}
