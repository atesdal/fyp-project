#ifndef M_ENGINE_HPP
#define M_ENGINE_HPP

#include "Engine.hpp"
#include "MessengerModule.hpp"

class MessageEngine : public Engine, public MessengerModule
{
public:
	MessageEngine(Messenger *parent);
	~MessageEngine();

private:
	void Update() override;
	void Interpret(Message &m) override;
	void HandleGraphicFeedback(Message &m);
	void HandleWindowFeedback(Message &m);
	void HandleGraphicInit(Message &m);
	void PackEvent(sf::Event &e);
};

#endif // !M_ENGINE_HPP
