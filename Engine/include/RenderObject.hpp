#ifndef RENDEROBJECT_HPP
#define RENDEROBJECT_HPP

namespace sf {
	class RenderWindow;
	class Color;
	class Shape;
	template<typename T> class Vector2;
	typedef Vector2<float> Vector2f;
}

class RenderObject
{
public:
	RenderObject();
	~RenderObject();

	void InitCircleShape(float pos_x, float pos_y, float circle_radius, sf::Color colour);
	void InitSquareShape(float pos_x, float pos_y, float size, sf::Color colour);
	void Draw(sf::RenderWindow *window) const;
	bool Translate(float dir_x, float dir_y) const;
	void SetActive(bool status);
	bool IsActive() const { return is_active_; }

	sf::Vector2f GetPos() const;

private:
	sf::Shape *shape_;
	bool is_active_;
};

#endif // !RENDEROBJECT_HPP

