#ifndef MESSENGER_HPP
#define MESSENGER_HPP

#include "Message.hpp"

#include <unordered_map>
#include <queue>
#include <mutex>
#include <condition_variable>

enum class ModuleName {
	Application,
	Engine
};

class Messenger
{
public:
	Messenger();
	~Messenger();

	void Send(ModuleName recipient, Message &message);
	void Receive(Message &message);
	void Read(Message &message);
	bool TryRead(Message &message);
	void AddRecipient(ModuleName name, Messenger *recipient);

private:
	std::unordered_map<ModuleName, Messenger*> recipients_;
	std::queue<Message> inbox_;
	mutable std::mutex m_;
	std::condition_variable c_;
};

#endif // !MESSENGER_HPP

