#ifndef MESSAGE_HPP
#define MESSAGE_HPP

#include <vector>
#include <string>

class Message
{
public:
	enum class Type {
		KeyInput,
		WindowEvent,
		GraphicFeedback,
		WindowFeedback,
		GraphicInit
	};

public:
	Message();
	Message(Type t);
	Message(Type t, const std::vector<std::string> &args);
	virtual ~Message();

	Type GetType() const { return t_; }
	int GetArgc() const { return argc_; }
	void AddArg(std::string arg);
	const std::vector<std::string>& GetArgs() { return args_; }

private:
	Type t_;
	unsigned int argc_{ 0 };
	std::vector<std::string> args_;
};

#endif // !MESSAGE_HPP
