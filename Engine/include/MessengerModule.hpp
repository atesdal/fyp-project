#ifndef MESSENGER_MODULE_HPP
#define MESSENGER_MODULE_HPP

#include "Messenger.hpp"
#include "Message.hpp"

class MessengerModule
{
public:
	MessengerModule(ModuleName name);
	virtual ~MessengerModule();

	void Receive();
	void ReceiveAll();
	virtual void Interpret(Message &m) = 0;

protected:
	ModuleName name_;
	Messenger *messenger_;
};

#endif // !MESSENGER_MODULE_HPP
