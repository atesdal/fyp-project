#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <vector>

namespace sf {
	class RenderWindow;
	class Event;
}

class RenderObject;

class Engine
{
public:
	Engine();
	virtual ~Engine();

	void Init(
		unsigned short int w_width,
		unsigned short int w_height,
		const char *w_name);

	void Close();

	void AddRectangleShape(
		unsigned int &handle,
		float pos_x,
		float pos_y,
		float size,
		int r,
		int g,
		int b);

	void AddCircleShape(
		unsigned int &handle,
		float pos_x,
		float pos_y,
		float circle_radius,
		int r,
		int g,
		int b);
	
protected:
	virtual void Update() = 0;
	void Draw();

protected:
	sf::RenderWindow *window_;
	std::vector<RenderObject*> renderobjects_;
};

#endif // !ENGINE_HPP
