#include "Game_fwd.hpp"
#include <iostream>

int main(int argc, const char **argv)
{
	Application *a = new Application();
	a->Run();
	delete a;
	std::cin.get();
	return 0;
}
